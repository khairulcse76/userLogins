<?php
session_start();

?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Registration Form</title>
        <link rel="stylesheet" href="style.css" type="text/css"/>
    </head>
    <body>
        <div class="wrapper">
            <div class="header"> <h3>OOP Login Registration System</h3> </div>
           
            
            <div class="content"><h2>Login</h2>
                
                <p class="msg">
                    <?php
                     if(isset($_SESSION['emty_msg']))
                        {
                            echo $_SESSION['emty_msg'];
                            unset($_SESSION['emty_msg']);
                        }
                    ?>
                    
                </p>
               
                
                <div class="login_reg">
                    <form action="loginrequest.php" method="POST">
                        <table>
                             <tr>
                                <td>Username</td>
                                <td><input type="text" name="username" required="your username here" placeholder="Please give your Username"></td>
                            </tr>
                             <tr>
                                <td>Password</td>
                                <td><input type="password" name="password" placeholder="Please give your password"></td>
                            </tr>
                            
                             <tr>
                                 <td colspan="2"><input type="submit" value="Login">
                                     <input type="reset" value="Reset"></td>
                            </tr>

                        </table>
                    </form>
                </div>
                <div class="back"><a href="dashboard.php" style="text-decoration: none;">BACK</a></div>
            </div>
             <div class="footer">www.maxbagworld.com</div>
        </div>
    </body>
</html>
