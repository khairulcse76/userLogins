<?php
    include_once '../vendor/autoload.php';
    
    use UserApps\user;
    
    $newobject= new user();
    $data= $newobject->userprofile();
    
//    echo "<pre>";
//    print_r($data);
            
    
?>


<html>
    <head>
        <meta charset="UTF-8">
        <title>Registration Form</title>
        <link rel="stylesheet" href="profilestyleSheet.css" type="text/css"/>
        
        <style>
            
            .hedlin{
                font-weight: bold;
                font-size: 20px;
                width: 50%;
            }
        </style>
    </head>
    <body>
         <div class="header"> <h3>OOP Login Registration System</h3> </div>
        <div class="wrapper">
           
            <div class="mainmenu">
                <ul>
                    <li><a href="dashboard.php">Home</a></li>
                    <li><a href="profile.php">Show Profile</a></li>
                    <li><a href="#">Logout</a></li>
                    <li><a href="profileupdate.php">Update your profile</a></li>
                </ul>
            </div>
            <div class="content">
                <div class="login_reg">
                    <?php 
                    if(isset($_SESSION['error_msg']))
                    { ?>
                    <p style="color: red; text-align:  center; font-size: 16px;"><?php
                    
                    echo $_SESSION['error_msg'];
                    unset($_SESSION['error_msg']);
                    ?></p>
                     <?php } ?>
                    
                    <form action="updateprofile.php" method="POST">
                        <table style="width: 60%">
                            
                            <tr>
                                <td class="hedlin">Name</td>
                                <td class="value"><?php echo $data['first_name']." ".$data['last_name']; ?></td>
                            </tr>
                            <tr>
                                <td class="hedlin">Personal Phone</td>
                                <td class="value"><?php echo $data['personal_mobile']; ?></td>
                            </tr>
                            <tr>
                                <td class="hedlin">Home Phone</td>
                                <td class="value"><?php echo $data['home_phone']; ?></td>
                            </tr>
                            <tr>
                                <td class="hedlin">Address</td>
                                <td class="value"><?php echo $data['address']; ?></td>
                            </tr>
                            <tr>
                                <td class="hedlin">Nationality</td>
                                <td class="value"><?php echo $data['nationality']; ?></td>
                            </tr>
                            <tr>
                                <td class="hedlin">Religion</td>
                                <td class="value"><?php echo $data['Religion']; ?></td>
                            </tr>
                             <tr>
                                <td class="hedlin">Blood Group</td>
                                <td class="value"><?php echo $data['blode_group']; ?></td>
                            </tr>
                            <tr>
                                <td class="hedlin">Gender</td>
                                <td class="value"><?php echo $data['gender']; ?></td>
                            </tr>
                            <tr>
                                <td class="hedlin">Date of Birth</td>
                                <td class="value"><?php echo $data['dateofbirth']; ?></td>
                            </tr>
                            <tr>
                                <td class="hedlin">National Id</td>
                                <td class="value"><?php echo $data['nationalID']; ?></td>
                            </tr>
                            <tr>
                                <td class="hedlin">Last Education</td>
                                <td class="value"><?php echo $data['lastEducationalStatus']; ?></td>
                            </tr>
                            <tr>
                                <td class="hedlin">Occupation</td>
                                <td class="value"><?php echo $data['occupation']; ?></td>
                            </tr>
                            
                        </table>
                    </form>
                    
          
                </div>
            </div>
            
            </div>
            <div class="footer">www.maxbagworld.com</div> 
    </body>
</html>
<?php // } ?>