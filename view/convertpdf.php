<?php
include_once '../vendor/mpdf/mpdf/mpdf.php';
include_once '../vendor/autoload.php';

use UserApps\user;

$objt=new user();

$alldata=$objt->userprofile();

$trs='';

$serial=0;

foreach ($alldata as $data):

    $serial++;
    $trs.="<tr>";
    $trs.="<td>".$serial."</td>";
    $trs.="<td>".$data['id']."</td>";
    $trs.="<td>".$data['first_name']." ".$data['last_name']."</td>";
    $trs.="<td>".$data['personal_mobile']."</td>";
    $trs.="<td>".$data['home_phone']."</td>";
    $trs.="<td>".$data['address']."</td>";
    $trs.="<td>".$data['nationality']."</td>";
    $trs.="<td>".$data['Religion']."</td>";
    $trs.="</tr>";
    endforeach;
    
    $html = <<<EOD
  <html>
    <head>
        <meta charset="UTF-8">
        <title>list of profile</title>
            <style>
                table {
                    border-collapse: collapse;
                        width: 100%;
                    }

                    th, td {
                        text-align: left;
                        padding: 8px;
                    }

                    tr:nth-child(even){background-color: #f2f2f2}

                    th {
                        background-color: #4CAF50;
                        color: white;
                }
            </style>
    </head>
    <body>
        <table>
        <thead>
            <tr>
                <th>SL</th>
                <th>ID</th>
                <th>Name</th>
                <th>personal mobile</th>
                <th>home phone</th>
                <th>Address</th>
                <th>nationality</th>
                <th>Religion</th>
            </tr>
    </thead>
    
    <tbody>
        $trs
    </tbody>
        </table>
    </body>
</html> 
            
            
EOD;
    
    $mpdff=new mPDF();
    $mpdff->WriteHTML($html);
    $mpdff->Output();
    exit();
 ?>

//