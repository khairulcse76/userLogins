<?php
session_start();
//echo $_GET['email'];
?>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Registration Form</title>
        <link rel="stylesheet" href="profilestyleSheet.css" type="text/css"/>
    </head>
    <body>
         <div class="header"> <h3>OOP Login Registration System</h3> </div>
        <div class="wrapper">
           
            <div class="mainmenu">
                <ul>
                    <li><a href="dashboard.php">Home</a></li>
                    <li><a href="profile.php">Show Profile</a></li>
                    <li><a href="#">Change password</a></li>
                    <li><a href="login.php">Login</a></li>
                    <li><a href="#">Logout</a></li>
                    <li><a href="Register.php">Register</a></li>
                </ul>
            </div>
            <div class="content">
                <div class="login_reg">
                    <?php 
                    if(isset($_SESSION['error_msg']))
                    { ?>
                    <p style="color: red; text-align:  center; font-size: 16px;"><?php
                    
                    echo $_SESSION['error_msg'];
                    unset($_SESSION['error_msg']);
                    ?></p>
                    <?php }
                    
                    ?>
                    
                    <form action="updateprofile.php" method="POST">
                        <table>
                            <tr>
                                <td>First Name</td>
                                <td><input type="text" name="fname" placeholder="First name"></td>
                            </tr>
                            <tr>
                                 <td>Last Name</td>
                                <td><input type="text" name="lname" placeholder="Last name"></td>
                            </tr>
                             <tr>
                                <td>Personal phone</td>
                                <td><input type="text" name="personalPhone" placeholder="Personal Phone Number"></td>
                            </tr>
                            <tr>
                                 <td>Home phone</td>
                                <td><input type="text" name="homePhone" placeholder="Home Phone Number"></td>
                            </tr>
                            
                             <tr>
                                <td>Nationality</td>
                                <td><input type="text" name="nationality" ></td>
                             </tr>
                             <tr>
                                 <td>Religion</td>
                                <td><input type="text" name="religion" ></td>
                             </tr>
                          
                             <tr>
                                <td>Blood Group</td>
                                <td><input type="text" name="bldGroup"></td>
                            </tr>
                            <tr>
                                  <td>Gender</td>
                                 <td><input type="radio" name="gender" value="Male"><label>Male</label>
                                     <input type="radio" name="gender" value="Female"><label>Female</label></td>
                            </tr>
                           
                             <tr>
                                <td>Date of Birth</td>
                                <td><input type="text" name="dateofbirth" placeholder="Date of Birth"></td>
                            </tr>
                            <tr>
                                  <td>NID</td>
                                <td><input type="text" name="nationalid" placeholder="National Id"></td>
                            </tr>
                            
                              <tr>
                                <td>Last Education Status</td>
                                <td><input type="text" name="ledustatus" placeholder="Please give your Education Status"></td>
                            </tr>
                            <tr>
                                  <td>Occupation</td>
                                  <td><select name="occupation" style="width: 285px; padding: 5px">
                                                  <option>Select One</option>
                                                   <option>Teacher</option>
                                                    <option>Professor</option>
                                                     <option>Engineer</option>
                                                      <option>Doctors</option>
                                                       <option>Employee</option>
                                      </select></td>
                            </tr>
                             <tr>
                                <td>Present Address</td>
                                <td><textarea  rows="3" cols="35" name="presentaddress"></textarea></td>
                            </tr>
                           
                            <tr>
                                 <td>Permanent Address</td>
                                <td><textarea  rows="3" cols="35" name="permanentaddress"></textarea></td>
                            </tr>
                            <tr>
                                  <td>zip code</td>
                                 <td><input type="text" name="zipcode" style="width: 150px;" ></td>
                            </tr>
                            <tr>
                                <td>Post code</td>
                                <td><input type="text" name="postcode" style="width: 150px;"  ></td>
                            </tr>
                            
                              <tr>
                                  <td colspan="2" class="child"><input type="submit" value="Register">
                                     <input type="reset" value="Reset"></td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
            
            </div>
            <div class="footer">www.maxbagworld.com</div> 
    </body>
</html>
