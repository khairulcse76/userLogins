<?php
error_reporting(E_ALL);
error_reporting(E_ALL & ~E_DEPRECATED);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

if (PHP_SAPI == 'cli')
    die('This example should only be run from a Web Browser');


require_once'../vendor/phpoffice/phpexcel/Classes/PHPExcel.php';
require_once ('../vendor/autoload.php');
use UserApps\user;

$obj= new user();
$allData= $obj->userprofile();

$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
        
    ->setLastModifiedBy("Maarten Balliauw")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Test result file");


$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1', 'SL')
    ->setCellValue('B1', 'ID')
    ->setCellValue('C1', 'Name')
    ->setCellValue('D1', 'Personal Phone')
    ->setCellValue('E1', 'Home Phone')
    ->setCellValue('F1', 'Address')
    ->setCellValue('G1', 'Nationality');

$counter=2;
$serial=0;

foreach($allData as $data){
    $serial++;
$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A'.$counter, $serial)
    ->setCellValue('B'.$counter, $data['id'])
    ->setCellValue('C'.$counter, $data['first_name']." ".$data['last_name'])
    ->setCellValue('D'.$counter, $data['personal_mobile'])
    ->setCellValue('E'.$counter, $data['home_phone'])
    ->setCellValue('F'.$counter, $data['address'])
    ->setCellValue('G'.$counter, $data['nationality']);
    $counter++;

}
 
$objPHPExcel->getActiveSheet()->setTitle('User Data list');


$objPHPExcel->setActiveSheetIndex(0);


header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="01simple.xls"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); 
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
header ('Cache-Control: cache, must-revalidate');
header ('Pragma: public');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
