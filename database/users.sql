-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 27, 2016 at 03:52 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `khairul`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `Varification` varchar(100) NOT NULL,
  `is_active` int(255) NOT NULL,
  `is_admin` int(50) NOT NULL,
  `created` varchar(255) NOT NULL,
  `modified` varchar(50) NOT NULL,
  `deleted` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `unique_id`, `username`, `password`, `email`, `Varification`, `is_active`, `is_admin`, `created`, `modified`, `deleted`) VALUES
(7, '579618d39e2a2', 'khairuli', 'khairul', 'khairulislamtpi76@gmail.com', '579618d39e2a8', 0, 0, '07:49:07PM 25-07-2016', '07:49:07PM 25-07-2016', 0),
(8, '5796194dee3f6', 'manik', 'manikislam', 'manik@gmail.com', '5796194dee3fd', 0, 0, '07:51:09PM 25-07-2016', '07:51:09PM 25-07-2016', 0),
(9, '57962ae7d7287', 'banglade', 'store()', 'bangladesh@yahoo.com', '57962ae7d728f', 0, 0, '09:06:15PM 25-07-2016', '09:06:15PM 25-07-2016', 0),
(10, '57962cd0a4268', 'content', 'content', 'content@gamil.com', '57962cd0a4271', 0, 0, '09:14:24PM 25-07-2016', '09:14:24PM 25-07-2016', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
