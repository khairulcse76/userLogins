-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 01, 2016 at 02:51 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `khairul`
--

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE IF NOT EXISTS `profiles` (
`id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `first_name` varchar(256) NOT NULL,
  `last_name` varchar(256) NOT NULL,
  `personal_mobile` int(20) NOT NULL,
  `home_phone` int(20) NOT NULL,
  `address` varchar(500) NOT NULL,
  `nationality` varchar(256) NOT NULL,
  `Religion` varchar(256) NOT NULL,
  `blode_group` varchar(256) NOT NULL,
  `gender` varchar(256) NOT NULL,
  `picture` varchar(256) NOT NULL,
  `dateofbirth` varchar(111) NOT NULL,
  `nationalID` varchar(100) NOT NULL,
  `lastEducationalStatus` varchar(256) NOT NULL,
  `occupation` varchar(256) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `users_id`, `first_name`, `last_name`, `personal_mobile`, `home_phone`, `address`, `nationality`, `Religion`, `blode_group`, `gender`, `picture`, `dateofbirth`, `nationalID`, `lastEducationalStatus`, `occupation`, `created`, `modified`, `deleted`) VALUES
(1, 1, 'Khairul', 'Islam', 1774460981, 1784983611, 'nothing', 'bangladeshi', 'islam', 'B+', 'male', 'khairul', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 12, 'khairul', 'islam', 2147483647, 1784983612, 'somthing', 'bangladesh', 'islam', 'A+', 'female', 'nothing', '09/11/1994', 'nothing', 'CSE', 'Student', '2016-07-30 02:04:06', '2016-07-30 03:05:05', '2016-07-30 03:04:02');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
